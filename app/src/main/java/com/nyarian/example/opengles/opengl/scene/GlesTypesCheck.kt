package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DistinctTypesInGroupException
import com.nyarian.example.opengles.opengl.vertex.Figure

class GlesTypesCheck(private val givenFigures: Array<out Figure>) {

    fun perform(): Int {
        val types = givenFigures
            .asSequence()
            .map(Figure::type)
            .distinct()
            .toList()
        if (types.size > 1) {
            throw DistinctTypesInGroupException("A single type is permitted, but the group have these:$types.")
        } else {
            return types.single()
        }
    }

}