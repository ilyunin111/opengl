package com.nyarian.example.opengles.opengl.program

import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.vertex.VertexBuffer

interface Program {

    fun validate()

    fun use()

    fun pointVariable(
        name: String,
        parameters: ShaderPointableVariable.ActivationParameters,
        vertexBuffer: VertexBuffer
    )

    fun getVariableLocation(name: String): Int
}