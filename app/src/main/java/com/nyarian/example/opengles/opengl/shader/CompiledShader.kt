package com.nyarian.example.opengles.opengl.shader

import android.opengl.GLES20.*
import com.nyarian.example.opengles.opengl.check.CheckException
import com.nyarian.example.opengles.opengl.check.ShaderCheck
import com.nyarian.example.opengles.opengl.program.PlainProgram
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable

class CompiledShader(
    internal val pointableVariables: Array<ShaderPointableVariable>,
    internal val variables: Array<ShaderVariable>,
    private val type: Int,
    code: String
) {

    val id: Int

    init {
        id = glCreateShader(type)
        if (id == 0) {
            throw ShaderCreationException("Could not create a shader for type $type")
        }
        glShaderSource(id, code)
        glCompileShader(id)
        checkCompilationResult(id, type, code)
    }

    private fun checkCompilationResult(id: Int, type: Int, code: String) {
        try {
            ShaderCheck(id, GL_COMPILE_STATUS).perform()
        } catch (e: CheckException) {
            glDeleteShader(id)
            throw ShaderCompilationException(
                glGetShaderInfoLog(id)
                    ?: "Could not compile shader with id $id, type $type. Code: ${System.lineSeparator()}$code"
            )
        }
    }

    fun link(other: CompiledShader): PlainProgram = when {
        this.type == GL_VERTEX_SHADER -> linkWithFragmentShader(other)
        this.type == GL_FRAGMENT_SHADER -> linkWithVertexShader(other)
        else -> throw IllegalShaderTypeException("Can't create program for a shader with type $type")
    }

    private fun linkWithFragmentShader(other: CompiledShader): PlainProgram {
        if (other.type == GL_FRAGMENT_SHADER) {
            return PlainProgram(this, other)
        } else {
            throwConfigurationException(other)
        }
    }

    private fun linkWithVertexShader(other: CompiledShader): PlainProgram {
        if (other.type == GL_VERTEX_SHADER) {
            return PlainProgram(other, this)
        } else {
            throwConfigurationException(other)
        }
    }

    private fun throwConfigurationException(other: CompiledShader): Nothing {
        throw ProgramConfigurationException("Can't link shader with type $type with a shader with type $other.type")
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CompiledShader

        if (type != other.type) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type
        result = 31 * result + id
        return result
    }

    override fun toString(): String {
        return "CompiledShader(type=$type, id=$id)"
    }


}
