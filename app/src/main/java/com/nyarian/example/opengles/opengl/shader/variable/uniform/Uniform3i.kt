package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform3i

class Uniform3i(private val x: Int, private val y: Int, private val z: Int) : Uniform {

    override fun point(location: Int) {
        glUniform3i(location, x, y, z)
    }

}