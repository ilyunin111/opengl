package com.nyarian.example.opengles.resource

import android.content.res.Resources
import java.io.InputStreamReader

class TextResource(private val resources: Resources, private val resourceId: Int) {

    fun asString(): String =
        buildString {
            InputStreamReader(resources.openRawResource(resourceId)).useLines {
                it.fold(this) { builder, string -> builder.append(string).append(System.lineSeparator()) }
            }
        }

}