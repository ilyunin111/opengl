package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DistinctComponentsCountException
import com.nyarian.example.opengles.opengl.vertex.Figure

class ComponentsCheck(private val givenFigures: Array<out Figure>) {

    fun perform(): Int {
        val components = givenFigures
            .asSequence()
            .map(Figure::components)
            .distinct()
            .toList()
        if (components.size > 1) {
            throw DistinctComponentsCountException(
                "All the vertices inside a single group should have a single " +
                        "components count per vertex, but these counts were given: $components"
            )
        } else {
            return components.single()
        }
    }

}