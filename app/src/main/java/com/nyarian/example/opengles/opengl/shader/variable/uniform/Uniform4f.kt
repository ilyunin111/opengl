package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform4f

class Uniform4f(private val x: Float, private val y: Float, private val z: Float, private val w: Float) : Uniform {

    override fun point(location: Int) {
        glUniform4f(location, x, y, z, w)
    }

}