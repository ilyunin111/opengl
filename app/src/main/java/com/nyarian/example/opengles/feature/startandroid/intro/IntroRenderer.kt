package com.nyarian.example.opengles.feature.startandroid.intro

import android.content.res.Resources
import android.opengl.GLES20.glLineWidth
import android.opengl.GLSurfaceView
import com.nyarian.example.opengles.R
import com.nyarian.example.opengles.opengl.attribute.TwoDCoordinate
import com.nyarian.example.opengles.opengl.attribute.coordinate
import com.nyarian.example.opengles.opengl.buffer.Buffers
import com.nyarian.example.opengles.opengl.render.SurfaceColor
import com.nyarian.example.opengles.opengl.render.Viewport
import com.nyarian.example.opengles.opengl.scene.Group
import com.nyarian.example.opengles.opengl.scene.Scene
import com.nyarian.example.opengles.opengl.shader.Shader
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable.ActivationParameters
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.opengl.shader.variable.VariableType
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA
import com.nyarian.example.opengles.opengl.shader.variable.uniform.uniform
import com.nyarian.example.opengles.opengl.vertex.*
import com.nyarian.example.opengles.resource.TextResource
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class IntroRenderer(private val resources: Resources) : GLSurfaceView.Renderer {

    private val buffers = Buffers()

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        SurfaceColor(RGBA.BLACK).clear()
        val vertexShader = Shader.vertex(
            textResource = TextResource(resources, R.raw.start_android_vertex_shader),
            pointableVariables = arrayOf(
                ShaderPointableVariable(
                    ATTRIBUTE_POSITION,
                    TwoDCoordinate.COMPONENTS,
                    TwoDCoordinate.POSITION,
                    VariableType.ATTRIBUTE
                )
            )
        ).compile()
        val fragmentShader = Shader.fragment(
            textResource = TextResource(resources, R.raw.start_android_fragment_shader),
            variables = arrayOf(ShaderVariable(UNIFORM_COLOR, VariableType.UNIFORM))
        ).compile()
        val program = vertexShader.link(fragmentShader)
        program.use()
        val vertexBuffer = VertexBuffer(scene)
        program.pointVariable(ATTRIBUTE_POSITION, ActivationParameters(0, false), vertexBuffer)
        uniform(RGBA.GREEN).point(program.getVariableLocation(UNIFORM_COLOR))
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Viewport(width, height).update()
    }

    override fun onDrawFrame(gl: GL10?) {
        buffers.clearColorBuffer()
        glLineWidth(5f)
        scene.draw()
    }

    companion object {
        private const val ATTRIBUTE_POSITION = "a_Position"
        private const val UNIFORM_COLOR = "u_Color"
        private val scene = Scene(
            Group(
                Triangle(-0.5f, -0.2f, 0.0f, 0.2f, 0.5f, -0.2f),
                Triangle(-0.6f, 0.2f, -0.2f, 0.2f, -0.2f, 0.8f)
            ),
            Group(
                TriangleStrip(
                    coordinate(0.1f, 0.8f),
                    coordinate(0.1f, 0.2f),
                    coordinate(0.5f, 0.8f),
                    coordinate(0.5f, 0.2f)
                )
            ),
            Group(
                TriangleFan(
                    coordinate(0.0f, -0.5f),
                    coordinate(-0.4f, -0.1f),
                    coordinate(0.4f, -0.1f),
                    coordinate(0.8f, -0.5f),
                    coordinate(0.4f, -0.9f),
                    coordinate(-0.4f, -0.9f),
                    coordinate(-0.8f, -0.5f)
                )
            ),
            Group(
                Line(
                    coordinate(-1f, -1f),
                    coordinate(1f, 1f)
                ),
                Line(
                    coordinate(1f, -1f),
                    coordinate(-1f, 1f)
                ),
                Line(
                    coordinate(0f, -1f),
                    coordinate(0f, 1f)
                ),
                Line(
                    coordinate(-1f, 0f),
                    coordinate(1f, 0f)
                )
            )
        )
    }

}
