package com.nyarian.example.opengles.opengl.projection

interface Projection {

    val matrix: Matrix

    fun bind(matrixLocation: Int)

}