package com.nyarian.example.opengles.opengl.texture

interface Texture {

    fun bind()

    fun unbind()

    fun setToFirstUnit()

    fun delete()

}