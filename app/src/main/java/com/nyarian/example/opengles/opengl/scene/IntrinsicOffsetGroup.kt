package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DrawableFigure

class IntrinsicOffsetGroup(private val delegate: DrawableFigure) : DrawableFigure by delegate {

    override fun draw(offset: Int): Int {
        delegate.draw(0)
        return offset
    }

}