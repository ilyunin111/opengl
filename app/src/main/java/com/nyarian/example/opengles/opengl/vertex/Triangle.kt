package com.nyarian.example.opengles.opengl.vertex

import android.opengl.GLES20.GL_TRIANGLES
import com.nyarian.example.opengles.opengl.attribute.Attribute
import com.nyarian.example.opengles.opengl.attribute.coordinate
import com.nyarian.example.opengles.opengl.attribute.mergeAttributes
import com.nyarian.example.opengles.opengl.attribute.strides
import com.nyarian.example.opengles.opengl.scene.StrideCheck

class Triangle(first: Attribute, second: Attribute, third: Attribute) : Figure {

    constructor(x1: Float, y1: Float, x2: Float, y2: Float, x3: Float, y3: Float) : this(
        coordinate(x1, y1),
        coordinate(x2, y2),
        coordinate(x3, y3)
    )

    override val attributes: FloatArray by lazy {
        mergeAttributes(first, second, third)
    }

    override val components: Int = first.components

    override val type: Int get() = GL_TRIANGLES

    override val vertices: Int
        get() = TRIANGLE_VERTICES

    override val stride: Int

    init {
        if (first.javaClass != second.javaClass || first.javaClass != third.javaClass) {
            throw IllegalArgumentException(
                "Line can't have attributes of different types, but got" +
                        " ${first.javaClass}, ${second.javaClass} and ${third.javaClass}"
            )
        }
        stride = StrideCheck(sequenceOf(first, second, third).strides()).perform()
    }

    companion object {
        private const val TRIANGLE_VERTICES = 3
    }

}
