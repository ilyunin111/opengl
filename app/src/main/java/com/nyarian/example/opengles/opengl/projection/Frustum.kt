package com.nyarian.example.opengles.opengl.projection

import android.opengl.GLES20.glUniformMatrix4fv
import android.opengl.Matrix.frustumM

class Frustum(
    private val matrixUniformLocation: Int,
    private val properties: FrustumProperties,
    private val viewportConfiguration: ViewportConfiguration
) {

    private val _matrix by lazy {
        val arrayMatrix = FloatArray(16)
        with(properties) {
            with(viewportConfiguration) {
                frustumM(
                    arrayMatrix,
                    offset,
                    if (isLandscape) left * aspectRatio else left,
                    if (isLandscape) right * aspectRatio else right,
                    if (isPortrait) bottom * aspectRatio else bottom,
                    if (isPortrait) top * aspectRatio else top,
                    near,
                    far
                )
            }
        }
        arrayMatrix
    }

    val matrix: Matrix get() = Matrix(_matrix)

    fun initialize() {
        glUniformMatrix4fv(matrixUniformLocation, 1, false, _matrix, 0)
    }

}
