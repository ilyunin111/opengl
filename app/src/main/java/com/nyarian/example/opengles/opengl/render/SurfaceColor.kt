package com.nyarian.example.opengles.opengl.render

import android.opengl.GLES20.*
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA

class SurfaceColor(private val rgba: RGBA) {

    fun clear() {
        with(rgba) { glClearColor(red, green, blue, alpha) }
    }

    fun clearBuffer() {
        glClear(GL_COLOR_BUFFER_BIT)
    }

}
