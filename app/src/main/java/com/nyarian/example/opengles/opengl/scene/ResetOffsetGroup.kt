package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DrawableFigure

class ResetOffsetGroup(private val delegate: DrawableFigure) : DrawableFigure by delegate {

    override fun draw(offset: Int): Int {
        return delegate.draw(0)
    }

}