package com.nyarian.example.opengles.opengl.vertex

import com.nyarian.example.opengles.opengl.attribute.Attribute

class AttributeComponentsCheck(private val givenVertices: Array<out Attribute>) {

    fun perform(): Int {
        val components = givenVertices
            .asSequence()
            .map(Attribute::components)
            .distinct()
            .toList()
        if (components.size > 1) {
            throw DistinctComponentsCountException(
                "All the vertices inside a single group should have a single " +
                        "components count per vertex, but these counts were given: $components"
            )
        } else {
            return components.single()
        }
    }

}