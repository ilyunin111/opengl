package com.nyarian.example.opengles.feature.startandroid.intro

import android.content.res.Resources
import android.opengl.GLES20.*
import android.opengl.GLSurfaceView
import com.nyarian.example.opengles.R
import com.nyarian.example.opengles.opengl.attribute.ThreeDCoordinate
import com.nyarian.example.opengles.opengl.attribute.coordinate
import com.nyarian.example.opengles.opengl.buffer.Buffers
import com.nyarian.example.opengles.opengl.program.PlainProgram
import com.nyarian.example.opengles.opengl.projection.Frustum
import com.nyarian.example.opengles.opengl.projection.FrustumProperties
import com.nyarian.example.opengles.opengl.projection.ViewportConfiguration
import com.nyarian.example.opengles.opengl.render.SurfaceColor
import com.nyarian.example.opengles.opengl.render.Viewport
import com.nyarian.example.opengles.opengl.scene.Scene
import com.nyarian.example.opengles.opengl.scene.colorUniformGroup
import com.nyarian.example.opengles.opengl.shader.Shader
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.ATTRIBUTE
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.UNIFORM
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA
import com.nyarian.example.opengles.opengl.shader.variable.uniform.UniformPointer
import com.nyarian.example.opengles.opengl.shader.variable.uniform.uniform
import com.nyarian.example.opengles.opengl.vertex.Triangle
import com.nyarian.example.opengles.opengl.vertex.VertexBuffer
import com.nyarian.example.opengles.resource.TextResource
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


class SceneRenderer172(private val resources: Resources) : GLSurfaceView.Renderer {

    private val buffers = Buffers()
    private lateinit var program: PlainProgram

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        glEnable(GL_DEPTH_TEST)
        SurfaceColor(RGBA.BLACK).clear()
        val vertexShader = Shader.vertex(
            TextResource(resources, R.raw.start_android_vertex_shader_172),
            arrayOf(
                ShaderPointableVariable(A_POSITION, ThreeDCoordinate.COMPONENTS, ThreeDCoordinate.POSITION, ATTRIBUTE)
            ),
            arrayOf(ShaderVariable(U_MATRIX, UNIFORM))
        ).compile()
        val fragmentShader =
            Shader.fragment(
                textResource = TextResource(resources, R.raw.start_android_fragment_shader_172),
                variables = arrayOf(ShaderVariable(U_COLOR, UNIFORM))
            ).compile()
        program = vertexShader.link(fragmentShader)
        program.use()
        val vertexBuffer = VertexBuffer(scene)
        program.pointVariable(
            A_POSITION,
            ShaderPointableVariable.ActivationParameters(scene.stride, false), vertexBuffer
        )
        colorUniform.pointer = program.getVariableLocation(U_COLOR)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Viewport(width, height).update()
        Frustum(
            program.getVariableLocation(U_MATRIX),
            FrustumProperties(0, -1.0F, 1.0F, -1.0F, 1.0F, 1.0F, 8.0F),
            ViewportConfiguration(width, height, resources.configuration.orientation)
        )
            .initialize()
    }

    override fun onDrawFrame(gl: GL10?) {
        buffers.clearColorAndDepthBuffers()
        glLineWidth(5f)
        scene.draw()
    }

    companion object {
        private const val A_POSITION = "a_Position"
        private const val U_MATRIX = "u_Matrix"
        private const val U_COLOR = "u_Color"
        private val colorUniform = UniformPointer()
        private val scene = Scene(
            Triangle(
                coordinate(-0.7F, -0.5F, -1.0F),
                coordinate(0.3F, -0.5F, -1.0F),
                coordinate(-0.2F, 0.3F, -1.0F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.PURPLE)),
            Triangle(
                coordinate(-0.3F, -0.4F, -1.1F),
                coordinate(0.7F, -0.4F, -1.1F),
                coordinate(0.2F, 0.4F, -1.1F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.YELLOW))
        )
    }

}
