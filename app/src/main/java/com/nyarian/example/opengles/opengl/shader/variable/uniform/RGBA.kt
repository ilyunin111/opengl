package com.nyarian.example.opengles.opengl.shader.variable.uniform

import com.nyarian.example.opengles.opengl.attribute.RGB
import com.nyarian.example.opengles.opengl.attribute.RGB.Companion.ABSENT
import com.nyarian.example.opengles.opengl.attribute.RGB.Companion.STRONGEST

data class RGBA(val red: Float, val green: Float, val blue: Float, val alpha: Float) {

    constructor(rgb: RGB, alpha: Float) : this(rgb.red, rgb.green, rgb.blue, alpha)

    init {
        arrayOf(red, green, blue, alpha)
            .forEach {
                if (it < 0 || it > 1) throw IllegalColorStrengthException(
                    "SurfaceColor strength can't have a value $it"
                )
            }
    }

    val asArray: FloatArray by lazy { floatArrayOf(red, green, blue, alpha) }

    companion object {
        val WHITE = RGBA(STRONGEST, STRONGEST, STRONGEST, STRONGEST)
        val RED = RGBA(STRONGEST, ABSENT, ABSENT, STRONGEST)
        val GREEN = RGBA(ABSENT, STRONGEST, ABSENT, STRONGEST)
        val BLUE = RGBA(ABSENT, ABSENT, STRONGEST, STRONGEST)
        val YELLOW = RGBA(STRONGEST, STRONGEST, ABSENT, STRONGEST)
        val PURPLE = RGBA(STRONGEST, ABSENT, STRONGEST, STRONGEST)
        val LIGHT_BLUE = RGBA(ABSENT, STRONGEST, STRONGEST, STRONGEST)
        val BLACK = RGBA(ABSENT, ABSENT, ABSENT, ABSENT)
    }

}
