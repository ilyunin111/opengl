package com.nyarian.example.opengles.opengl.buffer

import android.opengl.GLES20.*

class Buffers {

    fun clearColorBuffer() {
        glClear(GL_COLOR_BUFFER_BIT)
    }

    fun clearDepthBuffer() {
        glClear(GL_DEPTH_BUFFER_BIT)
    }

    fun clearColorAndDepthBuffers() {
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
    }

}
