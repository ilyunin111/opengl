package com.nyarian.example.opengles.opengl.attribute

import com.nyarian.example.opengles.opengl.backbone.BYTES_PER_FLOAT

class FourDCoordinate(val x: Float, val y: Float, val z: Float, val w: Float) : Attribute, Coordinate {

    override val asFloatArray: FloatArray by lazy { floatArrayOf(x, y, z, w) }
    override val components: Int get() = COMPONENTS
    override val stride: Int get() = STRIDE

    constructor(x: Float, y: Float) : this(x, y, 0F, 1F)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FourDCoordinate

        if (x != other.x) return false
        if (y != other.y) return false
        if (z != other.z) return false
        if (w != other.w) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + z.hashCode()
        result = 31 * result + w.hashCode()
        return result
    }

    override fun toString(): String {
        return "FourDCoordinate(x=$x, y=$y, z=$z, w=$w)"
    }

    companion object {
        const val POSITION = 0
        const val COMPONENTS = 4
        const val STRIDE = COMPONENTS * BYTES_PER_FLOAT
    }

}
