package com.nyarian.example.opengles.feature.startandroid.intro

import android.content.res.Resources
import android.opengl.GLES20.*
import android.opengl.GLSurfaceView
import android.os.SystemClock
import com.nyarian.example.opengles.R
import com.nyarian.example.opengles.opengl.attribute.ThreeDCoordinate
import com.nyarian.example.opengles.opengl.attribute.coordinate
import com.nyarian.example.opengles.opengl.buffer.Buffers
import com.nyarian.example.opengles.opengl.program.PlainProgram
import com.nyarian.example.opengles.opengl.projection.Frustum
import com.nyarian.example.opengles.opengl.projection.FrustumProperties
import com.nyarian.example.opengles.opengl.projection.ViewMatrix
import com.nyarian.example.opengles.opengl.projection.ViewportConfiguration
import com.nyarian.example.opengles.opengl.render.SurfaceColor
import com.nyarian.example.opengles.opengl.render.Viewport
import com.nyarian.example.opengles.opengl.scene.Scene
import com.nyarian.example.opengles.opengl.scene.colorUniformGroup
import com.nyarian.example.opengles.opengl.shader.Shader
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.ATTRIBUTE
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.UNIFORM
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA
import com.nyarian.example.opengles.opengl.shader.variable.uniform.UniformPointer
import com.nyarian.example.opengles.opengl.shader.variable.uniform.uniform
import com.nyarian.example.opengles.opengl.vertex.Line
import com.nyarian.example.opengles.opengl.vertex.Triangle
import com.nyarian.example.opengles.opengl.vertex.VertexBuffer
import com.nyarian.example.opengles.resource.TextResource
import java.lang.Math.cos
import java.lang.Math.sin
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import kotlin.math.PI

class SceneRenderer173(private val resources: Resources) : GLSurfaceView.Renderer {

    private val buffers = Buffers()
    private var viewMatrix = ViewMatrix(
        0,
        coordinate(2F, -2F, 4F),
        coordinate(0F, 0F, 0F),
        coordinate(0F, 1F, 0F)
    )
    private lateinit var frustum: Frustum
    private lateinit var program: PlainProgram

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        glEnable(GL_DEPTH_TEST)
        SurfaceColor(RGBA.BLACK).clear()
        val vertexShader = Shader.vertex(
            TextResource(resources, R.raw.start_android_vertex_shader_172),
            arrayOf(
                ShaderPointableVariable(A_POSITION, ThreeDCoordinate.COMPONENTS, ThreeDCoordinate.POSITION, ATTRIBUTE)
            ),
            arrayOf(ShaderVariable(U_MATRIX, UNIFORM))
        ).compile()
        val fragmentShader =
            Shader.fragment(
                textResource = TextResource(resources, R.raw.start_android_fragment_shader_172),
                variables = arrayOf(ShaderVariable(U_COLOR, UNIFORM))
            ).compile()
        program = vertexShader.link(fragmentShader)
        program.use()
        val vertexBuffer = VertexBuffer(scene)
        program.pointVariable(
            A_POSITION,
            ShaderPointableVariable.ActivationParameters(scene.stride, false), vertexBuffer
        )
        colorUniform.pointer = program.getVariableLocation(U_COLOR)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Viewport(width, height).update()
        frustum = Frustum(
            program.getVariableLocation(U_MATRIX),
            FrustumProperties(0, -1.0F, 1.0F, -1.0F, 1.0F, 2.0F, 8.0F),
            ViewportConfiguration(width, height, resources.configuration.orientation)
        )
        val cameraMatrix = viewMatrix.matrix.multiply(frustum.matrix)
        cameraMatrix.bind(program.getVariableLocation(U_MATRIX))
    }

    override fun onDrawFrame(gl: GL10?) {
        recreateViewMatrix()
        buffers.clearColorAndDepthBuffers()
        glLineWidth(5F)
        scene.draw()
    }

    private fun recreateViewMatrix() {
        createViewMatrix()
        viewMatrix.matrix.multiply(frustum.matrix).bind(program.getVariableLocation(U_MATRIX))
    }

    private fun createViewMatrix() {
        val time = (SystemClock.uptimeMillis().toFloat() % TIME) / TIME
        val angle = time * 2 * PI
        viewMatrix = ViewMatrix(
            0,
            coordinate((cos(angle) * 4F).toFloat(), 1F, (sin(angle) * 4F).toFloat()),
            coordinate(0F, 0F, 0F),
            coordinate(0F, 1F, 0F)
        )
    }

    companion object {
        private const val TIME = 10000L
        private const val A_POSITION = "a_Position"
        private const val U_MATRIX = "u_Matrix"
        private const val U_COLOR = "u_Color"
        private val colorUniform = UniformPointer()
        private val scene = Scene(
            Triangle(
                coordinate(-0.8F, -0.4F, 0.9F),
                coordinate(0.8F, -0.4F, 0.9F),
                coordinate(0F, 0.4F, 0.9F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.GREEN)),
            Triangle(
                coordinate(-0.8F, -0.4F, -0.9F),
                coordinate(0.8F, -0.4F, -0.9F),
                coordinate(0F, 0.4F, -0.9F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.BLUE)),
            Triangle(
                coordinate(0.9F, -0.4F, -0.8F),
                coordinate(0.9F, -0.4F, 0.8F),
                coordinate(0.9F, 0.4F, 0F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.RED)),
            Triangle(
                coordinate(-0.9F, -0.4F, -0.8F),
                coordinate(-0.9F, -0.4F, 0.8F),
                coordinate(-0.9F, 0.4F, 0F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.YELLOW)),
            Line(
                coordinate(-3F, 0F, 0F),
                coordinate(3F, 0F, 0F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.LIGHT_BLUE)),
            Line(
                coordinate(0F, -3F, 0F),
                coordinate(0F, 3F, 0F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.PURPLE)),
            Line(
                coordinate(0F, 0F, -3F),
                coordinate(0F, 0F, 3F)
            ).colorUniformGroup(colorUniform, uniform(RGBA(1.0F, 0.5F, 0.0F, 1.0F)))
        )
    }

}
