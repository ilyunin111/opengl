package com.nyarian.example.opengles.opengl.shader.variable

enum class VariableType {
    UNIFORM, ATTRIBUTE, VARYING
}
