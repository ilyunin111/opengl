package com.nyarian.example.opengles.opengl.shader.variable.uniform

fun uniform(x: Int) = Uniform1i(x)
fun uniform(x: Int, y: Int) = Uniform2i(x, y)
fun uniform(x: Int, y: Int, z: Int) = Uniform3i(x, y, z)
fun uniform(x: Int, y: Int, z: Int, w: Int) = Uniform4i(x, y, z, w)

fun uniform(x: Float) = Uniform1f(x)
fun uniform(x: Float, y: Float) = Uniform2f(x, y)
fun uniform(x: Float, y: Float, z: Float) = Uniform3f(x, y, z)
fun uniform(x: Float, y: Float, z: Float, w: Float) = Uniform4f(x, y, z, w)
fun uniform(rgba: RGBA) = with(rgba) { Uniform4f(red, green, blue, alpha) }