package com.nyarian.example.opengles.opengl.shader.variable.uniform

class IllegalColorStrengthException(message: String) : IllegalArgumentException(message)
