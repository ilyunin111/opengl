package com.nyarian.example.opengles.opengl.program

class ProgramValidationException(message: String) : RuntimeException(message)
