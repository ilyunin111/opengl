package com.nyarian.example.opengles.opengl.shader

class ProgramLinkingException(message: String) : IllegalStateException(message)
