package com.nyarian.example.opengles.opengl.shader.variable.attribute

import android.opengl.GLES20.glVertexAttribPointer
import com.nyarian.example.opengles.opengl.shader.variable.ActivationStrategy
import com.nyarian.example.opengles.opengl.shader.variable.LocatingStrategy
import java.nio.Buffer

class AttributeActivationStrategy(private val locatingStrategy: LocatingStrategy) :
    ActivationStrategy {

    override fun activate(location: Int, size: Int, type: Int, normalized: Boolean, stride: Int, ptr: Buffer) {
        glVertexAttribPointer(location, size, type, normalized, stride, ptr)
    }

    override fun getLocation(programId: Int, name: String): Int = locatingStrategy.getLocation(programId, name)
}
