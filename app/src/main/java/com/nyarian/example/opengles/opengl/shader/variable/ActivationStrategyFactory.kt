package com.nyarian.example.opengles.opengl.shader.variable

import com.nyarian.example.opengles.opengl.shader.variable.attribute.AttributeActivationStrategy
import com.nyarian.example.opengles.opengl.shader.variable.attribute.AttributeLocatingStrategy

class ActivationStrategyFactory {

    fun create(variableType: VariableType): ActivationStrategy = when (variableType) {
        VariableType.ATTRIBUTE, VariableType.VARYING -> AttributeActivationStrategy(AttributeLocatingStrategy())
        VariableType.UNIFORM -> throw IllegalArgumentException("Uniform variables are now bindable to attr arrays.")
    }

}
