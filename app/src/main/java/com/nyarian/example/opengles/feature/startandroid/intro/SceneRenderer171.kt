package com.nyarian.example.opengles.feature.startandroid.intro

import android.content.res.Resources
import android.opengl.GLES20.glLineWidth
import android.opengl.GLSurfaceView
import com.nyarian.example.opengles.R
import com.nyarian.example.opengles.opengl.attribute.*
import com.nyarian.example.opengles.opengl.buffer.Buffers
import com.nyarian.example.opengles.opengl.program.PlainProgram
import com.nyarian.example.opengles.opengl.render.SurfaceColor
import com.nyarian.example.opengles.opengl.render.Viewport
import com.nyarian.example.opengles.opengl.scene.Group
import com.nyarian.example.opengles.opengl.scene.Scene
import com.nyarian.example.opengles.opengl.scene.group
import com.nyarian.example.opengles.opengl.shader.Shader
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.ATTRIBUTE
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA
import com.nyarian.example.opengles.opengl.vertex.*
import com.nyarian.example.opengles.resource.TextResource
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class SceneRenderer171(private val resources: Resources) : GLSurfaceView.Renderer {

    private val buffers = Buffers()
    private lateinit var program: PlainProgram

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        SurfaceColor(RGBA.BLACK).clear()
        val vertexShader = Shader.vertex(
            textResource = TextResource(resources, R.raw.start_android_vertex_shader_171),
            pointableVariables = arrayOf(
                ShaderPointableVariable(
                    ATTRIBUTE_POSITION,
                    TwoDCoordinate.COMPONENTS,
                    TwoDCoordinate.POSITION,
                    ATTRIBUTE
                ),
                ShaderPointableVariable(ATTRIBUTE_COLOR, Color.COMPONENTS, TwoDCoordinate.COMPONENTS, ATTRIBUTE)
            )
        ).compile()
        val fragmentShader =
            Shader.fragment(textResource = TextResource(resources, R.raw.start_android_fragment_shader_171)).compile()
        program = vertexShader.link(fragmentShader)
        program.use()
        val vertexBuffer = VertexBuffer(scene)
        program.pointVariable(
            ATTRIBUTE_POSITION,
            ShaderPointableVariable.ActivationParameters(scene.stride, false), vertexBuffer
        )
        program.pointVariable(
            ATTRIBUTE_COLOR,
            ShaderPointableVariable.ActivationParameters(scene.stride, false), vertexBuffer
        )
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Viewport(width, height).update()
    }

    override fun onDrawFrame(gl: GL10?) {
        buffers.clearColorBuffer()
        glLineWidth(5f)
        scene.draw()
    }

    companion object {
        private const val ATTRIBUTE_POSITION = "a_Position"
        private const val ATTRIBUTE_COLOR = "a_Color"
        private val scene = Scene(
            Group(
                Triangle(
                    coordinate(-0.9f, 0.9f).colored(RGB.RED),
                    coordinate(-0.9f, 0.7f).colored(RGB.GREEN),
                    coordinate(-0.8f, 0.9f).colored(RGB.BLUE)
                ),
                Triangle(
                    coordinate(-0.8f, 0.7f).colored(RGB.RED),
                    coordinate(-0.7f, 0.7f).colored(RGB.GREEN),
                    coordinate(-0.7f, 0.9f).colored(RGB.BLUE)
                ),
                Triangle(
                    coordinate(0.1f, 0.7f).colored(RGB.RED),
                    coordinate(0.3f, 0.9f).colored(RGB.GREEN),
                    coordinate(0.1f, 0.9f).colored(RGB.BLUE)
                ),
                Triangle(
                    coordinate(0.1f, 0.7f).colored(RGB.RED),
                    coordinate(0.3f, 0.7f).colored(RGB.BLUE),
                    coordinate(0.3f, 0.9f).colored(RGB.GREEN)
                )
            ),
            Line(
                coordinate(-0.9f, 0.1f).colored(RGB.RED),
                coordinate(0.9f, 0.1f).colored(RGB.GREEN)
            ).group(),
            Line(
                coordinate(-0.8f, 0.0f).colored(RGB.RED),
                coordinate(0.8f, 0.0f).colored(RGB.GREEN)
            ).group(),
            Group(
                Point(coordinate(-0.7f, -0.1f).colored(RGB.RED)),
                Point(coordinate(-0.3f, -0.1f).colored(RGB.RED)),
                Point(coordinate(0.1f, -0.1f).colored(RGB.RED))
            ),
            LineStrip(
                coordinate(-0.5f, -0.5f).colored(RGB.RED),
                coordinate(-0.3f, -0.5f).colored(RGB.GREEN),
                coordinate(-0.3f, -0.3f).colored(RGB.BLUE),
                coordinate(-0.1f, -0.3f).colored(RGB.YELLOW),
                coordinate(-0.1f, -0.1f).colored(RGB.PURPLE)
            ).group(),
            LineLoop(
                coordinate(0.5f, -0.5f).colored(RGB.RED),
                coordinate(0.4f, -0.5f).colored(RGB.GREEN),
                coordinate(0.4f, -0.4f).colored(RGB.BLUE),
                coordinate(0.3f, -0.4f).colored(RGB.YELLOW),
                coordinate(0.3f, -0.3f).colored(RGB.PURPLE)
            ).group()
        )
    }

}
