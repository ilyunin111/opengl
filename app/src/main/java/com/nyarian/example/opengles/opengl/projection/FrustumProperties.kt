package com.nyarian.example.opengles.opengl.projection

data class FrustumProperties(
    val offset: Int,
    val left: Float,
    val right: Float,
    val bottom: Float,
    val top: Float,
    val near: Float,
    val far: Float
)
