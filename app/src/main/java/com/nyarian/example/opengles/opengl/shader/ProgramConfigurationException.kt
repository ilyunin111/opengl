package com.nyarian.example.opengles.opengl.shader

class ProgramConfigurationException(message: String) : RuntimeException(message)