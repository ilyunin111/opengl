package com.nyarian.example.opengles.opengl.projection

import android.opengl.GLES20.glUniformMatrix4fv
import android.opengl.Matrix.orthoM

class OrthographicProjection(proportions: Proportions) : Projection {

    private val _matrix = FloatArray(16)
    override val matrix = Matrix(_matrix)

    constructor(orientation: Int, height: Int, width: Int) : this(Proportions(orientation, height, width))

    init {
        with(proportions) { orthoM(_matrix, 0, left, right, bottom, top, near, far) }
    }

    override fun bind(matrixLocation: Int) {
        glUniformMatrix4fv(matrixLocation, 1, false, _matrix, 0)
    }

}