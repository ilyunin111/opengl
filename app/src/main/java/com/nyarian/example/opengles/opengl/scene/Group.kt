package com.nyarian.example.opengles.opengl.scene

import android.opengl.GLES20.glDrawArrays
import com.nyarian.example.opengles.opengl.vertex.*

class Group<in T : Figure>(private vararg val givenVertices: T) : DrawableFigure {

    override val attributes: FloatArray by lazy { Merge(givenVertices).merged }
    override val type: Int
    override val vertices: Int
    override val components: Int
    override val stride: Int

    init {
        type = GlesTypesCheck(givenVertices).perform()
        components = ComponentsCheck(givenVertices).perform()
        stride = StrideCheck(givenVertices.asSequence().strides()).perform()
        vertices = givenVertices.sum()
    }

    override fun draw(offset: Int): Int {
        glDrawArrays(type, offset, vertices)
        return vertices + offset
    }

}
