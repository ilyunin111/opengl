package com.nyarian.example.opengles.opengl.projection

import android.opengl.GLES20.glUniformMatrix4fv
import java.lang.Math.tan
import kotlin.math.PI

class PerspectiveProjection(
    fieldOfViewDegrees: Float,
    aspectRatio: Float,
    near: Float,
    far: Float
) : Projection {

    private val _matrix: FloatArray
    override val matrix: Matrix

    constructor(
        fieldOfViewDegrees: Float,
        proportions: Proportions,
        near: Float,
        far: Float
    ) : this(fieldOfViewDegrees, proportions.aspectRatio, near, far)

    init {
        val angleInRadians = (fieldOfViewDegrees * PI / 180).toFloat()
        val focalLength = (1.0 / tan(angleInRadians / 2.0)).toFloat()
        _matrix = floatArrayOf(
            focalLength / aspectRatio, 0f, 0f, 0f,
            0f, focalLength, 0f, 0f,
            0f, 0f, -((far + near) / (far - near)), -1f,
            0f, 0f, -((2f * far * near) / (far - near)), 0f
        )
        matrix = Matrix(_matrix)
    }

    override fun bind(matrixLocation: Int) {
        glUniformMatrix4fv(matrixLocation, 1, false, _matrix, 0)
    }

}
