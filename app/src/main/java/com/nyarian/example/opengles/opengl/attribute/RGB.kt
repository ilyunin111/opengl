package com.nyarian.example.opengles.opengl.attribute

import com.nyarian.example.opengles.opengl.shader.variable.uniform.IllegalColorStrengthException

data class RGB(val red: Float, val green: Float, val blue: Float) {

    init {
        arrayOf(red, green, blue)
            .forEach {
                if (it < 0 || it > 1) throw IllegalColorStrengthException(
                    "SurfaceColor strength can't have a value $it"
                )
            }
    }

    val asArray: FloatArray by lazy { floatArrayOf(red, green, blue) }

    companion object {
        val STRONGEST = 1.0f
        val ABSENT = 0.0f
        val WHITE = RGB(STRONGEST, STRONGEST, STRONGEST)
        val RED = RGB(STRONGEST, ABSENT, ABSENT)
        val GREEN = RGB(ABSENT, STRONGEST, ABSENT)
        val BLUE = RGB(ABSENT, ABSENT, STRONGEST)
        val YELLOW = RGB(STRONGEST, STRONGEST, ABSENT)
        val PURPLE = RGB(STRONGEST, ABSENT, STRONGEST)
        val WHOLLY_ABSENT = RGB(ABSENT, ABSENT, ABSENT)
    }

}
