package com.nyarian.example.opengles.opengl.vertex

import android.opengl.GLES20.GL_TRIANGLE_STRIP
import com.nyarian.example.opengles.opengl.attribute.Attribute
import com.nyarian.example.opengles.opengl.attribute.mergeAttributes
import com.nyarian.example.opengles.opengl.attribute.strides
import com.nyarian.example.opengles.opengl.scene.StrideCheck

class TriangleStrip(private vararg val attrs: Attribute) : Figure {

    override val attributes: FloatArray by lazy { mergeAttributes(*attrs) }
    override val components: Int = AttributeComponentsCheck(attrs).perform()
    override val stride: Int = StrideCheck(attrs.asSequence().strides()).perform()
    override val type: Int
        get() = GL_TRIANGLE_STRIP
    override val vertices: Int
        get() = attrs.size

    init {
        if (attrs.size < 3) {
            throw IllegalArgumentException("Triangle strip attributes size can't be less than 3, but got ${attrs.size}")
        }
        AttributesTypeCheck(attrs).perform()
    }

}