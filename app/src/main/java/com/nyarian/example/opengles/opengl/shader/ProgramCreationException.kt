package com.nyarian.example.opengles.opengl.shader

class ProgramCreationException(message: String) : RuntimeException(message)