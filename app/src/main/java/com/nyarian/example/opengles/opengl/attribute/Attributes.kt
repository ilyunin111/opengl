package com.nyarian.example.opengles.opengl.attribute

fun mergeAttributes(vararg attributes: Attribute): FloatArray = attributes.asSequence()
    .map(Attribute::asFloatArray)
    .fold(FloatArray(0), FloatArray::plus)

fun Sequence<Attribute>.strides() = map(Attribute::stride)
    .toList()
    .toIntArray()

fun Attribute.withTexture(s: Float, t: Float): CompositeAttribute =
    CompositeAttribute(arrayOf(this, TextureCoordinate(s, t)))

fun Attribute.colored(rgb: RGB) = CompositeAttribute(arrayOf(this, Color(rgb)))