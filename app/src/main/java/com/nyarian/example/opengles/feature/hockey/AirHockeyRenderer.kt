package com.nyarian.example.opengles.feature.hockey

import android.content.res.Resources
import android.opengl.GLSurfaceView
import com.nyarian.example.opengles.R
import com.nyarian.example.opengles.R.raw.*
import com.nyarian.example.opengles.opengl.attribute.*
import com.nyarian.example.opengles.opengl.buffer.Buffers
import com.nyarian.example.opengles.opengl.program.PlainProgram
import com.nyarian.example.opengles.opengl.program.Program
import com.nyarian.example.opengles.opengl.projection.Matrix
import com.nyarian.example.opengles.opengl.projection.PerspectiveProjection
import com.nyarian.example.opengles.opengl.projection.Projection
import com.nyarian.example.opengles.opengl.projection.Proportions
import com.nyarian.example.opengles.opengl.render.SurfaceColor
import com.nyarian.example.opengles.opengl.render.Viewport
import com.nyarian.example.opengles.opengl.scene.*
import com.nyarian.example.opengles.opengl.shader.CompiledShader
import com.nyarian.example.opengles.opengl.shader.Shader
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable.ActivationParameters
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.ATTRIBUTE
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.UNIFORM
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA
import com.nyarian.example.opengles.opengl.shader.variable.uniform.uniform
import com.nyarian.example.opengles.opengl.texture.GLTexture
import com.nyarian.example.opengles.opengl.texture.Texture
import com.nyarian.example.opengles.opengl.vertex.Point
import com.nyarian.example.opengles.opengl.vertex.TriangleFan
import com.nyarian.example.opengles.opengl.vertex.VertexBuffer
import com.nyarian.example.opengles.resource.TextResource
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class AirHockeyRenderer(private val resources: Resources) : GLSurfaceView.Renderer {

    private lateinit var scene: Scene
    private lateinit var colorProgram: Program
    private lateinit var textureProgram: Program
    private lateinit var projection: Projection
    private lateinit var tableTexture: Texture
    private val modelMatrix = Matrix()

    private val buffers = Buffers()
    private val orientation: Int get() = resources.configuration.orientation

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        SurfaceColor(RGBA.BLACK).clear()
        colorProgram = createColorProgram()
        textureProgram = createTextureProgram()
        tableTexture = GLTexture.bitmap(resources, R.drawable.hockey)
        val mallets = Group(
            Point(coordinate(0F, -0.4F).colored(RGB.GREEN)),
            Point(coordinate(0F, 0.4F).colored(RGB.RED))
        )
        val table = Group(
            TriangleFan(
                coordinate(0.0F, 0.0F).withTexture(0.5F, 0.5F),
                coordinate(-0.5F, -0.8F).withTexture(0F, 0.9F),
                coordinate(0.5F, -0.8F).withTexture(1F, 0.9F),
                coordinate(0.5F, 0.8F).withTexture(1F, 0.1F),
                coordinate(-0.5F, 0.8F).withTexture(0F, 0.1F)
            )
        )
        val tableBuffer = VertexBuffer(table)
        val malletsBuffer = VertexBuffer(mallets)
        scene = Scene(
            table
                .onDraw {
                    projection.bind(textureProgram.getVariableLocation(U_MATRIX))
                    tableTexture.apply {
                        setToFirstUnit()
                        bind()
                    }
                    uniform(0).point(textureProgram.getVariableLocation(U_TEXTURE_UNIT))
                    textureProgram.pointVariable(A_POS, ActivationParameters(table.stride), tableBuffer)
                    textureProgram.pointVariable(A_TEXTURE_COORDINATES, ActivationParameters(table.stride), tableBuffer)
                }
                .withProgram(textureProgram),
            mallets
                .intrinsicOffset()
                .onDraw {
                    projection.bind(colorProgram.getVariableLocation(U_MATRIX))
                    colorProgram.pointVariable(A_POS, ActivationParameters(mallets.stride), malletsBuffer)
                    colorProgram.pointVariable(A_COLOR, ActivationParameters(mallets.stride), malletsBuffer)
                }
                .withProgram(colorProgram)
        )
    }

    private fun createColorProgram(): PlainProgram {
        val vertexShader: CompiledShader =
            Shader.vertex(
                TextResource(resources, simple_vertex_shader),
                arrayOf(
                    ShaderPointableVariable(A_POS, TwoDCoordinate.COMPONENTS, TwoDCoordinate.POSITION, ATTRIBUTE),
                    ShaderPointableVariable(A_COLOR, Color.COMPONENTS, TwoDCoordinate.COMPONENTS, ATTRIBUTE)
                ),
                arrayOf(ShaderVariable(U_MATRIX, UNIFORM))
            ).compile()
        val fragmentShader: CompiledShader =
            Shader.fragment(TextResource(resources, simple_fragment_shader), emptyArray()).compile()
        return vertexShader.link(fragmentShader)
    }

    private fun createTextureProgram(): PlainProgram {
        val vertexShader: CompiledShader =
            Shader.vertex(
                TextResource(resources, texture_vertex_shader),
                arrayOf(
                    ShaderPointableVariable(A_POS, TwoDCoordinate.COMPONENTS, TwoDCoordinate.POSITION, ATTRIBUTE),
                    ShaderPointableVariable(
                        A_TEXTURE_COORDINATES,
                        TextureCoordinate.COMPONENTS,
                        TwoDCoordinate.COMPONENTS,
                        ATTRIBUTE
                    )
                ),
                arrayOf(ShaderVariable(U_MATRIX, UNIFORM))
            ).compile()
        val fragmentShader: CompiledShader =
            Shader.fragment(
                TextResource(resources, texture_fragment_shader),
                emptyArray(),
                arrayOf(ShaderVariable(U_TEXTURE_UNIT, UNIFORM))
            ).compile()
        return vertexShader.link(fragmentShader)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Viewport(width, height).update()
        projection = PerspectiveProjection(
            45F,
            Proportions(orientation, height, width),
            1F,
            10F
        )
        modelMatrix.identity()
            .translate(0F, 0.2F, -2.5F)
            .rotate(0, -45F, 1F, 0F, 0F)
        projection.matrix.mutableMultiply(modelMatrix)
    }

    override fun onDrawFrame(gl: GL10?) {
        buffers.clearColorBuffer()
        scene.draw()
    }

    companion object {

        const val A_POS = "a_Position"
        const val U_MATRIX = "u_Matrix"

        const val A_COLOR = "a_Color"

        const val U_TEXTURE_UNIT = "u_TextureUnit"
        const val A_TEXTURE_COORDINATES = "a_TextureCoordinates"
    }

}
