package com.nyarian.example.opengles.opengl.texture

class BitmapDecodingException(message: String) : RuntimeException(message)