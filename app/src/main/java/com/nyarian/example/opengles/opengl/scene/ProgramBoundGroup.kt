package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.program.Program
import com.nyarian.example.opengles.opengl.vertex.DrawableFigure
import com.nyarian.example.opengles.opengl.vertex.Merge
import com.nyarian.example.opengles.opengl.vertex.strides
import com.nyarian.example.opengles.opengl.vertex.sum

class ProgramBoundGroup(private val program: Program, private vararg val givenVertices: DrawableFigure) :
    DrawableFigure {

    override val attributes: FloatArray by lazy { Merge(givenVertices).merged }
    override val components: Int
    override val type: Int
    override val vertices: Int
    override val stride: Int

    init {
        type = GlesTypesCheck(givenVertices).perform()
        components = ComponentsCheck(givenVertices).perform()
        stride = StrideCheck(givenVertices.asSequence().strides()).perform()
        vertices = givenVertices.sum()
    }

    override fun draw(offset: Int): Int {
        program.use()
        return givenVertices.fold(offset) { accumulator, vertex -> vertex.draw(accumulator) }
    }
}
