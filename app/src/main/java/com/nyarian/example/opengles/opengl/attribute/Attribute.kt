package com.nyarian.example.opengles.opengl.attribute

interface Attribute {

    val asFloatArray: FloatArray

    val components: Int

    val stride: Int

}