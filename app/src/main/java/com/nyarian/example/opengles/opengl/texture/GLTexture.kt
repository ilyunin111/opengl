package com.nyarian.example.opengles.opengl.texture

import android.content.res.Resources
import android.opengl.GLES20.*

class GLTexture private constructor(private val type: Int) : Texture {

    val id: Int

    init {
        val textureIds = IntArray(1)
        glGenTextures(1, textureIds, 0)
        val generatedId = textureIds[0]
        if (generatedId == 0) {
            throw TextureGenerationException("Could not generate texture")
        } else {
            id = generatedId
        }
    }

    override fun bind() {
        glBindTexture(type, id)
    }

    override fun unbind() {
        glBindTexture(type, 0)
    }

    override fun setToFirstUnit() {
        glActiveTexture(GL_TEXTURE0)
    }

    override fun delete() {
        glDeleteTextures(1, intArrayOf(id), 0)
    }

    companion object {
        fun bitmap(
            resources: Resources,
            resourceId: Int,
            minifyingFilter: Int = GL_LINEAR_MIPMAP_LINEAR,
            magnifyingFilter: Int = GL_LINEAR
        ): Texture {
            return BitmapTexture(
                GLTexture(GL_TEXTURE_2D),
                resources,
                resourceId,
                TwoDTextureFilters(minifyingFilter, magnifyingFilter)
            )
        }
    }

}