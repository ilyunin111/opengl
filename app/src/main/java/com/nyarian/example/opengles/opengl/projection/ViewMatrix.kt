package com.nyarian.example.opengles.opengl.projection

import android.opengl.Matrix.setLookAtM
import com.nyarian.example.opengles.opengl.attribute.ThreeDCoordinate

class ViewMatrix(
    offset: Int,
    cameraPosition: ThreeDCoordinate,
    cameraDirection: ThreeDCoordinate,
    angle: ThreeDCoordinate
) {

    private val _matrix = FloatArray(16)
    val matrix: Matrix get() = Matrix(_matrix)

    init {
        setLookAtM(
            _matrix, offset,
            cameraPosition.x, cameraPosition.y, cameraPosition.z,
            cameraDirection.x, cameraDirection.y, cameraDirection.z,
            angle.x, angle.y, angle.z
        )
    }

}
