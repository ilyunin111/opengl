package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform4i

class Uniform4i(private val x: Int, private val y: Int, private val z: Int, private val w: Int) : Uniform {

    override fun point(location: Int) {
        glUniform4i(location, x, y, z, w)
    }

}