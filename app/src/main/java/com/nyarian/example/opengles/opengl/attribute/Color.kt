package com.nyarian.example.opengles.opengl.attribute

import com.nyarian.example.opengles.opengl.backbone.BYTES_PER_FLOAT

class Color(private val value: RGB) : Attribute {

    constructor(red: Float, green: Float, blue: Float) : this(RGB(red, green, blue))

    override val asFloatArray: FloatArray get() = value.asArray

    override val stride: Int
        get() = COLOR_STRIDE

    override val components: Int
        get() = COMPONENTS

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Color

        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toString(): String {
        return "Color(value=$value)"
    }

    companion object {
        const val COMPONENTS = 3
        private const val COLOR_STRIDE = COMPONENTS * BYTES_PER_FLOAT
    }

}