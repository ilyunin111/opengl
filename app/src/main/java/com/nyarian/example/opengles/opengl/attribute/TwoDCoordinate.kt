package com.nyarian.example.opengles.opengl.attribute

import com.nyarian.example.opengles.opengl.backbone.BYTES_PER_FLOAT

class TwoDCoordinate(val x: Float, val y: Float) : Attribute, Coordinate {

    override val asFloatArray: FloatArray by lazy { floatArrayOf(x, y) }
    override val components: Int get() = COMPONENTS
    override val stride: Int get() = STRIDE

    fun asSequence(): Sequence<Float> = sequenceOf(x, y)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TwoDCoordinate

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    override fun toString(): String {
        return "Coordinate(x=$x, y=$y)"
    }

    companion object {
        const val POSITION = 0
        const val COMPONENTS = 2
        const val STRIDE = COMPONENTS * BYTES_PER_FLOAT
    }

}
