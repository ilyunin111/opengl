package com.nyarian.example.opengles.opengl.shader.variable.uniform

interface Uniform {

    fun point(location: Int)

}