package com.nyarian.example.opengles.opengl.projection

import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT

class Proportions(private val orientation: Int, height: Int, width: Int) {

    val isLandscape: Boolean get() = orientation == ORIENTATION_LANDSCAPE

    val isPortrait: Boolean get() = orientation == ORIENTATION_PORTRAIT

    val aspectRatio: Float by lazy {
        when {
            isPortrait -> width.toFloat() / height.toFloat()
            isLandscape -> height.toFloat() / width.toFloat()
            else -> throw IllegalArgumentException("Undefined or square orientation is not supported. Current is $orientation")
        }
    }

    val left: Float = if (isLandscape) -aspectRatio else LOWER_BOUND

    val right: Float = if (isLandscape) aspectRatio else UPPER_BOUND

    val bottom: Float = if (isPortrait) -aspectRatio else LOWER_BOUND

    val top: Float = if (isPortrait) aspectRatio else UPPER_BOUND

    val near: Float = LOWER_BOUND

    val far: Float = UPPER_BOUND

    companion object {
        private const val LOWER_BOUND = -1F
        private const val UPPER_BOUND = 1F
    }

}