package com.nyarian.example.opengles.opengl.vertex

import android.opengl.GLES20.GL_TRIANGLE_FAN
import com.nyarian.example.opengles.opengl.attribute.Attribute
import com.nyarian.example.opengles.opengl.attribute.mergeAttributes
import com.nyarian.example.opengles.opengl.attribute.strides
import com.nyarian.example.opengles.opengl.scene.StrideCheck

class TriangleFan(private vararg val attrs: Attribute) : Figure {

    override val attributes: FloatArray by lazy { mergeAttributes(*attrs, attrs[1]) }

    override val components: Int = AttributeComponentsCheck(attrs).perform()

    override val type: Int
        get() = GL_TRIANGLE_FAN

    override val vertices: Int
        get() = attrs.size + 1

    override val stride =
        StrideCheck(attrs.asSequence().strides()).perform()

    init {
        if (attrs.size < 2) {
            throw IllegalArgumentException("Triangle fan attributes size can't be less than 3, but got ${attrs.size}")
        }
        AttributesTypeCheck(attrs).perform()
    }

}