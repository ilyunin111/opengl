package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DistinctComponentsCountException

class StrideCheck(private val givenStrides: IntArray) {

    fun perform(): Int {
        val strides = givenStrides
            .distinct()
            .toList()
        if (strides.size > 1) {
            throw DistinctComponentsCountException(
                "All the vertices inside a single group should have a single " +
                        "stride value, but these were given: $strides"
            )
        } else {
            return strides.single()
        }
    }


}