package com.nyarian.example.opengles.opengl.shader

class ShaderCreationException(message: String) : RuntimeException(message)
