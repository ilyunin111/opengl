package com.nyarian.example.opengles.opengl.projection

import android.opengl.GLES20.glUniformMatrix4fv
import android.opengl.Matrix.*

class Matrix(val array: FloatArray) {

    constructor() : this(FloatArray(16))

    fun bind(uniformLocation: Int) {
        glUniformMatrix4fv(uniformLocation, 1, false, array, 0)
    }

    fun identity(): Matrix = apply { setIdentityM(array, 0) }

    fun multiply(matrix: Matrix): Matrix {
        if (matrix.array.size != this.array.size) {
            throw IllegalArgumentException("This matrix's size is ${this.array.size} and other's is ${matrix.array.size}")
        }
        val result = FloatArray(matrix.array.size)
        multiplyMM(result, 0, matrix.array, 0, this.array, 0)
        return Matrix(result)
    }

    fun mutableMultiply(matrix: Matrix): Matrix {
        if (matrix.array.size != this.array.size) {
            throw IllegalArgumentException("This matrix's size is ${this.array.size} and other's is ${matrix.array.size}")
        }
        val temp = FloatArray(array.size)
        multiplyMM(temp, 0, this.array, 0, matrix.array, 0)
        System.arraycopy(temp, 0, array, 0, array.size)
        return this
    }

    fun onto(matrix: Matrix): Matrix {
        for (i in 0 until 16) {
            matrix.array[i] = array[i]
        }
        return matrix
    }

    fun rotate(offset: Int, angle: Float, x: Float, y: Float, z: Float): Matrix {
        rotateM(array, offset, angle, x, y, z)
        return this
    }

    fun translate(x: Float, y: Float, z: Float): Matrix {
        translateM(array, 0, x, y, z)
        return this
    }

}
