package com.nyarian.example.opengles.feature.startandroid.intro

import android.content.res.Resources
import android.opengl.GLES20.*
import android.opengl.GLSurfaceView
import android.os.SystemClock
import com.nyarian.example.opengles.R
import com.nyarian.example.opengles.opengl.attribute.ThreeDCoordinate
import com.nyarian.example.opengles.opengl.attribute.coordinate
import com.nyarian.example.opengles.opengl.buffer.Buffers
import com.nyarian.example.opengles.opengl.program.PlainProgram
import com.nyarian.example.opengles.opengl.projection.*
import com.nyarian.example.opengles.opengl.render.SurfaceColor
import com.nyarian.example.opengles.opengl.render.Viewport
import com.nyarian.example.opengles.opengl.scene.DrawablesGroup
import com.nyarian.example.opengles.opengl.scene.Scene
import com.nyarian.example.opengles.opengl.scene.colorUniformGroup
import com.nyarian.example.opengles.opengl.scene.onDraw
import com.nyarian.example.opengles.opengl.shader.Shader
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.ATTRIBUTE
import com.nyarian.example.opengles.opengl.shader.variable.VariableType.UNIFORM
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA
import com.nyarian.example.opengles.opengl.shader.variable.uniform.UniformPointer
import com.nyarian.example.opengles.opengl.shader.variable.uniform.uniform
import com.nyarian.example.opengles.opengl.vertex.Line
import com.nyarian.example.opengles.opengl.vertex.Triangle
import com.nyarian.example.opengles.opengl.vertex.VertexBuffer
import com.nyarian.example.opengles.resource.TextResource
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class SceneRenderer174(private val resources: Resources) : GLSurfaceView.Renderer {

    private val buffers = Buffers()
    private val viewMatrix = ViewMatrix(
        0,
        coordinate(2F, 2F, 3F),
        coordinate(0F, 0F, 0F),
        coordinate(0F, 1F, 0F)
    )
    private var modelMatrix = Matrix()
    private var mvpMatrix = Matrix()
    private lateinit var frustum: Frustum
    private lateinit var program: PlainProgram

    private val colorUniform = UniformPointer()
    private val scene = Scene(
        DrawablesGroup(
            Line(
                coordinate(-3F, 0F, 0F),
                coordinate(3F, 0F, 0F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.LIGHT_BLUE)),
            Line(
                coordinate(0F, -3F, 0F),
                coordinate(0F, 3F, 0F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.PURPLE)),
            Line(
                coordinate(0F, 0F, -3F),
                coordinate(0F, 0F, 3F)
            ).colorUniformGroup(colorUniform, uniform(RGBA.RED))
        ).onDraw {
            resetModelMatrix()
            recalculateMvpMatrix()
        },
        Triangle(
            coordinate(-1F, -0.5F, 0.5F),
            coordinate(1F, -0.5F, 0.5F),
            coordinate(0F, 0.5F, 0.5F)
        ).colorUniformGroup(colorUniform, uniform(RGBA.BLUE)).onDraw {
            resetModelMatrix()
            recalculateModelMatrix()
            recalculateMvpMatrix()
        }
    )

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        glEnable(GL_DEPTH_TEST)
        SurfaceColor(RGBA.BLACK).clear()
        val vertexShader = Shader.vertex(
            TextResource(resources, R.raw.start_android_vertex_shader_172),
            arrayOf(
                ShaderPointableVariable(A_POSITION, ThreeDCoordinate.COMPONENTS, ThreeDCoordinate.POSITION, ATTRIBUTE)
            ),
            arrayOf(ShaderVariable(U_MATRIX, UNIFORM))
        ).compile()
        val fragmentShader =
            Shader.fragment(
                textResource = TextResource(resources, R.raw.start_android_fragment_shader_172),
                variables = arrayOf(ShaderVariable(U_COLOR, UNIFORM))
            ).compile()
        program = vertexShader.link(fragmentShader)
        program.use()
        val vertexBuffer = VertexBuffer(scene)
        program.pointVariable(
            A_POSITION,
            ShaderPointableVariable.ActivationParameters(scene.stride, false), vertexBuffer
        )
        colorUniform.pointer = program.getVariableLocation(U_COLOR)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Viewport(width, height).update()
        frustum = Frustum(
            program.getVariableLocation(U_MATRIX),
            FrustumProperties(0, -1.0F, 1.0F, -1.0F, 1.0F, 2.0F, 8.0F),
            ViewportConfiguration(width, height, resources.configuration.orientation)
        )
        val cameraMatrix = viewMatrix.matrix.multiply(frustum.matrix)
        cameraMatrix.bind(program.getVariableLocation(U_MATRIX))
        recalculateMvpMatrix()
    }

    override fun onDrawFrame(gl: GL10?) {
        buffers.clearColorAndDepthBuffers()
        glLineWidth(5F)
        scene.draw()
    }

    private fun resetModelMatrix() {
        modelMatrix.identity()
    }

    private fun recalculateModelMatrix() {
        val angle = (SystemClock.uptimeMillis().toFloat() % TIME) / TIME * 360
        modelMatrix.rotate(0, angle, 0F, 1F, 1F)
    }

    private fun recalculateMvpMatrix() {
        modelMatrix.multiply(viewMatrix.matrix).multiply(frustum.matrix).onto(mvpMatrix)
            .bind(program.getVariableLocation(U_MATRIX))
    }

    companion object {
        private const val TIME = 10000L
        private const val A_POSITION = "a_Position"
        private const val U_MATRIX = "u_Matrix"
        private const val U_COLOR = "u_Color"
    }

}
