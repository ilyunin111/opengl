package com.nyarian.example.opengles.opengl.texture

class TextureGenerationException(message: String) : RuntimeException(message)
