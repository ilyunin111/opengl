package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DrawableFigure
import com.nyarian.example.opengles.opengl.vertex.Merge
import com.nyarian.example.opengles.opengl.vertex.strides
import com.nyarian.example.opengles.opengl.vertex.sum

class DrawablesGroup(private vararg val givenVertices: DrawableFigure) : DrawableFigure {

    override val attributes: FloatArray by lazy { Merge(givenVertices).merged }
    override val type: Int
    override val vertices: Int
    override val components: Int
    override val stride: Int

    init {
        type = GlesTypesCheck(givenVertices).perform()
        components = ComponentsCheck(givenVertices).perform()
        stride = StrideCheck(givenVertices.asSequence().strides()).perform()
        vertices = givenVertices.sum()
    }

    override fun draw(offset: Int): Int {
        return givenVertices.fold(offset) { accumulator, vertex -> vertex.draw(accumulator) }
    }

}
