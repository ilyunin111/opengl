package com.nyarian.example.opengles.opengl.shader.variable

class ShaderVariable(val name: String, variableType: VariableType) {

    private val locatingStrategy = LocatingStrategyFactory().create(variableType)

    fun getLocation(programId: Int): Int = locatingStrategy.getLocation(programId, name)
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ShaderVariable

        if (name != other.name) return false
        if (locatingStrategy != other.locatingStrategy) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + locatingStrategy.hashCode()
        return result
    }

    override fun toString(): String {
        return "ShaderVariable(name='$name', locatingStrategy=$locatingStrategy)"
    }

}