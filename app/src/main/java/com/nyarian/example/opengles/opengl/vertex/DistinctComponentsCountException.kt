package com.nyarian.example.opengles.opengl.vertex

class DistinctComponentsCountException(message: String) : RuntimeException(message)
