package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform2f

class Uniform2f(private val x: Float, private val y: Float) : Uniform {

    override fun point(location: Int) {
        glUniform2f(location, x, y)
    }

}