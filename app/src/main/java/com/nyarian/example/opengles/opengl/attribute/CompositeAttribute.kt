package com.nyarian.example.opengles.opengl.attribute

import java.util.*

class CompositeAttribute(private val attributes: Array<Attribute>) : Attribute {

    override val stride: Int by lazy { attributes.asSequence().map(Attribute::stride).sum() }

    override val asFloatArray: FloatArray by lazy { mergeAttributes(*attributes) }

    override val components: Int by lazy { attributes.asSequence().map(Attribute::components).sum() }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CompositeAttribute

        if (!attributes.contentEquals(other.attributes)) return false

        return true
    }

    override fun hashCode(): Int {
        return attributes.contentHashCode()
    }

    override fun toString(): String {
        return "CompositeAttribute(attributes=${Arrays.toString(attributes)})"
    }

}