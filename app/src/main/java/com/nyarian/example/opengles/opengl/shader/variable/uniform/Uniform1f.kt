package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform1f

class Uniform1f(private val x: Float) : Uniform {

    override fun point(location: Int) {
        glUniform1f(location, x)
    }

}