package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform3f

class Uniform3f(private val x: Float, private val y: Float, private val z: Float) : Uniform {

    override fun point(location: Int) {
        glUniform3f(location, x, y, z)
    }

}