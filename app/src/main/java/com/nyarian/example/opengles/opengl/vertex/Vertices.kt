package com.nyarian.example.opengles.opengl.vertex

fun Array<out Figure>.sum(): Int {
    return asSequence()
        .map(Figure::vertices)
        .sum()
}

fun Sequence<Figure>.strides(): IntArray = this.map(Figure::stride).toList().toIntArray()