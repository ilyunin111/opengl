package com.nyarian.example.opengles.opengl.check

import android.opengl.GLES20.GL_FALSE
import android.opengl.GLES20.glGetShaderiv

class ShaderCheck(private val shaderId: Int, private val flag: Int) {

    fun perform() {
        val result = IntArray(1)
        glGetShaderiv(shaderId, flag, result, 0)
        if (result[0] == GL_FALSE) {
            throw CheckException()
        }
    }

}