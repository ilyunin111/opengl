package com.nyarian.example.opengles.opengl.texture

import android.content.res.Resources
import android.graphics.BitmapFactory
import android.opengl.GLES20.GL_TEXTURE_2D
import android.opengl.GLES20.glGenerateMipmap
import android.opengl.GLUtils.texImage2D

class BitmapTexture(glTexture: GLTexture, resources: Resources, resourceId: Int, filters: TwoDTextureFilters) :
    Texture by glTexture {

    init {
        val options = BitmapFactory.Options()
        options.inScaled = false
        val bitmap = BitmapFactory.decodeResource(resources, resourceId, options)
        if (bitmap == null) {
            glTexture.delete()
            throw BitmapDecodingException("Could not decode resource with id $resourceId.")
        }
        glTexture.bind()
        filters.apply()
        texImage2D(GL_TEXTURE_2D, 0, bitmap, 0)
        bitmap.recycle()
        glGenerateMipmap(GL_TEXTURE_2D)
        glTexture.unbind()
    }

}