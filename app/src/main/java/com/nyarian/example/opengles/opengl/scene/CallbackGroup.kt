package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DrawableFigure

class CallbackGroup(private val delegate: DrawableFigure, private val onDraw: () -> Unit) : DrawableFigure by delegate {

    override fun draw(offset: Int): Int {
        onDraw()
        return delegate.draw(offset)
    }
}
