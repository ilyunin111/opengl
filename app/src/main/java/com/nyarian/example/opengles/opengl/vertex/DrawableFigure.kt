package com.nyarian.example.opengles.opengl.vertex

interface DrawableFigure : Figure {

    fun draw(offset: Int): Int

}
