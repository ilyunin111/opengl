package com.nyarian.example.opengles.opengl.projection

import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.content.res.Configuration.ORIENTATION_PORTRAIT

data class ViewportConfiguration(val width: Int, val height: Int, val orientation: Int) {

    val isPortrait: Boolean get() = orientation == ORIENTATION_PORTRAIT
    val isLandscape: Boolean get() = orientation == ORIENTATION_LANDSCAPE
    val aspectRatio: Float by lazy { if (isPortrait) height.toFloat() / width.toFloat() else width.toFloat() / height.toFloat() }

    init {
        if (!isPortrait && !isLandscape) {
            throw IllegalArgumentException("Non-portrait or landscape configuration is not supported")
        }
    }

}
