package com.nyarian.example.opengles.opengl.vertex

import android.opengl.GLES20.GL_LINES
import com.nyarian.example.opengles.opengl.attribute.Attribute
import com.nyarian.example.opengles.opengl.attribute.coordinate
import com.nyarian.example.opengles.opengl.attribute.mergeAttributes

class Line(first: Attribute, second: Attribute) : Figure {

    override val attributes: FloatArray by lazy { mergeAttributes(first, second) }

    override val components: Int = first.components

    override val type: Int get() = GL_LINES

    override val stride: Int = first.stride

    override val vertices: Int
        get() = LINE_VERTICES

    constructor(x1: Float, y1: Float, x2: Float, y2: Float) : this(coordinate(x1, y1), coordinate(x2, y2))

    init {
        if (first.javaClass != second.javaClass) {
            throw IllegalArgumentException(
                "Line can't have attributes of different types, but got" +
                        " ${first.javaClass} and ${second.javaClass}"
            )
        }
        if (first.components != second.components) {
            throw IllegalArgumentException(
                "Components count can't be different, but got ${first.components} and " +
                        "`${second.components}"
            )
        }
    }

    companion object {
        private const val LINE_VERTICES = 2
    }

}
