package com.nyarian.example.opengles.opengl.shader.variable

import android.opengl.GLES20.glEnableVertexAttribArray
import com.nyarian.example.opengles.opengl.vertex.VertexBuffer

class ShaderPointableVariable(
    val name: String,
    private val size: Int,
    private val position: Int,
    variableType: VariableType
) {

    private val activationStrategy: ActivationStrategy = ActivationStrategyFactory().create(variableType)

    fun adjustPointer(programId: Int, parameters: ActivationParameters, vertexBuffer: VertexBuffer) {
        val location = activationStrategy.getLocation(programId, name)
        vertexBuffer.position(position)
        with(parameters) {
            activationStrategy.activate(location, size, vertexBuffer.type, normalized, stride, vertexBuffer.buffer)
            glEnableVertexAttribArray(location)
        }
        vertexBuffer.position(0)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ShaderPointableVariable

        if (name != other.name) return false
        if (size != other.size) return false
        if (position != other.position) return false
        if (activationStrategy != other.activationStrategy) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + size
        result = 31 * result + position
        result = 31 * result + activationStrategy.hashCode()
        return result
    }

    override fun toString(): String {
        return "ShaderPointableVariable(name='$name', size=$size, position=$position, activationStrategy=$activationStrategy)"
    }


    data class ActivationParameters(val stride: Int, val normalized: Boolean = false)

}
