package com.nyarian.example.opengles.opengl.vertex

import android.opengl.GLES20.GL_FLOAT
import com.nyarian.example.opengles.opengl.scene.Scene
import java.nio.Buffer
import java.nio.ByteBuffer
import java.nio.ByteOrder

class VertexBuffer(array: FloatArray) {

    val buffer: Buffer

    val type: Int = GL_FLOAT

    constructor(scene: Scene) : this(scene.floatArray)

    constructor(drawableVertex: DrawableFigure) : this(drawableVertex.attributes)

    init {
        buffer = ByteBuffer
            .allocateDirect(array.size * FLOAT_BYTES)
            .order(ByteOrder.nativeOrder())
            .asFloatBuffer()
            .apply { put(array) }
            .apply { position(0) }
    }

    fun position(position: Int) {
        buffer.position(position)
    }

    companion object {
        private const val FLOAT_BYTES = 4
    }

}