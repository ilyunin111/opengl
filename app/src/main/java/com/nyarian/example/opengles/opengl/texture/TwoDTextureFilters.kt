package com.nyarian.example.opengles.opengl.texture

import android.opengl.GLES20.*

class TwoDTextureFilters(private val minifyingFilter: Int, private val magnifyingFilter: Int) {

    fun apply() {
        setTextureMinifyingFilter()
        setTextureMagnifyingFilter()
    }

    private fun setTextureMinifyingFilter() {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minifyingFilter)
    }

    private fun setTextureMagnifyingFilter() {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magnifyingFilter)
    }

}