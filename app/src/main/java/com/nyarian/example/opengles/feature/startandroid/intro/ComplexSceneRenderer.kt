package com.nyarian.example.opengles.feature.startandroid.intro

import android.content.res.Resources
import android.opengl.GLES20.glLineWidth
import android.opengl.GLSurfaceView
import com.nyarian.example.opengles.R
import com.nyarian.example.opengles.opengl.attribute.TwoDCoordinate
import com.nyarian.example.opengles.opengl.attribute.coordinate
import com.nyarian.example.opengles.opengl.buffer.Buffers
import com.nyarian.example.opengles.opengl.render.SurfaceColor
import com.nyarian.example.opengles.opengl.render.Viewport
import com.nyarian.example.opengles.opengl.scene.Scene
import com.nyarian.example.opengles.opengl.scene.UniformGroup
import com.nyarian.example.opengles.opengl.scene.colorUniformGroup
import com.nyarian.example.opengles.opengl.shader.Shader
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.opengl.shader.variable.VariableType
import com.nyarian.example.opengles.opengl.shader.variable.uniform.RGBA
import com.nyarian.example.opengles.opengl.shader.variable.uniform.UniformPointer
import com.nyarian.example.opengles.opengl.shader.variable.uniform.uniform
import com.nyarian.example.opengles.opengl.vertex.*
import com.nyarian.example.opengles.resource.TextResource
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class ComplexSceneRenderer(private val resources: Resources) : GLSurfaceView.Renderer {

    private val buffers = Buffers()

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        SurfaceColor(RGBA.BLACK).clear()
        val vertexShader = Shader.vertex(
            textResource = TextResource(resources, R.raw.start_android_vertex_shader),
            pointableVariables = arrayOf(
                ShaderPointableVariable(
                    ATTRIBUTE_POSITION,
                    TwoDCoordinate.COMPONENTS,
                    TwoDCoordinate.POSITION,
                    VariableType.ATTRIBUTE
                )
            )
        ).compile()
        val fragmentShader = Shader.fragment(
            textResource = TextResource(resources, R.raw.start_android_fragment_shader),
            variables = arrayOf(ShaderVariable(UNIFORM_COLOR, VariableType.UNIFORM))
        ).compile()
        val program = vertexShader.link(fragmentShader)
        program.use()
        val vertexBuffer = VertexBuffer(scene)
        program.pointVariable(
            ATTRIBUTE_POSITION,
            ShaderPointableVariable.ActivationParameters(0, false), vertexBuffer
        )
        colorUniformPointer.pointer = program.getVariableLocation(UNIFORM_COLOR)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        Viewport(width, height).update()
    }

    override fun onDrawFrame(gl: GL10?) {
        buffers.clearColorBuffer()
        glLineWidth(5f)
        scene.draw()
    }

    companion object {
        private const val ATTRIBUTE_POSITION = "a_Position"
        private const val UNIFORM_COLOR = "u_Color"
        private val colorUniformPointer = UniformPointer()
        private val scene = Scene(
            UniformGroup(
                colorUniformPointer,
                uniform(RGBA.BLUE),
                Triangle(-0.9f, 0.9f, -0.9f, 0.7f, -0.8f, 0.9f),
                Triangle(-0.8f, 0.7f, -0.7f, 0.7f, -0.7f, 0.9f),
                Triangle(0.1f, 0.7f, 0.3f, 0.9f, 0.1f, 0.9f),
                Triangle(0.1f, 0.7f, 0.3f, 0.7f, 0.3f, 0.9f)
            ),
            Line(-0.9f, 0.1f, 0.9f, 0.1f).colorUniformGroup(
                colorUniformPointer,
                uniform(RGBA.GREEN)
            ),
            Line(-0.8f, 0.0f, 0.8f, 0.0f).colorUniformGroup(
                colorUniformPointer,
                uniform(RGBA.YELLOW)
            ),
            UniformGroup(
                colorUniformPointer,
                uniform(RGBA.RED),
                Point(-0.7f, -0.1f),
                Point(-0.3f, -0.1f),
                Point(0.1f, -0.1f)
            ),
            LineStrip(
                coordinate(-0.5f, -0.5f),
                coordinate(-0.3f, -0.5f),
                coordinate(-0.3f, -0.3f),
                coordinate(-0.1f, -0.3f),
                coordinate(-0.1f, -0.1f)
            ).colorUniformGroup(
                colorUniformPointer,
                uniform(RGBA.WHITE)
            ),
            LineLoop(
                coordinate(0.5f, -0.5f),
                coordinate(0.4f, -0.5f),
                coordinate(0.4f, -0.4f),
                coordinate(0.3f, -0.4f),
                coordinate(0.3f, -0.3f)
            ).colorUniformGroup(
                colorUniformPointer,
                uniform(RGBA(0.5f, 0.5f, 0.5f, 1f))
            )
        )
    }

}
