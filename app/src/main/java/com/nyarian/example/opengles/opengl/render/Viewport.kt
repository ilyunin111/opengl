package com.nyarian.example.opengles.opengl.render

import android.opengl.GLES20.glViewport
import android.util.Size

class Viewport(private val coordinate: Coordinate, private val size: Size) {

    constructor(x: Int, y: Int, width: Int, height: Int) : this(Coordinate(x, y), Size(width, height))

    constructor(width: Int, height: Int) : this(Coordinate.CENTER, Size(width, height))

    fun update() {
        glViewport(coordinate.x, coordinate.y, size.width, size.height)
    }

}