package com.nyarian.example.opengles.opengl.vertex

interface Figure {

    val attributes: FloatArray

    val components: Int

    val type: Int

    val vertices: Int

    val stride: Int

}
