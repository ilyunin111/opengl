package com.nyarian.example.opengles.opengl.shader.variable.uniform

data class UniformPointer(var pointer: Int = NOT_INITIALIZED) {

    companion object {
        const val NOT_INITIALIZED = -1
    }

}
