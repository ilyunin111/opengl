package com.nyarian.example.opengles.opengl.shader.variable.attribute

import android.opengl.GLES20.glGetAttribLocation
import com.nyarian.example.opengles.opengl.shader.variable.LocatingStrategy

class AttributeLocatingStrategy : LocatingStrategy {

    override fun getLocation(programId: Int, name: String): Int {
        return glGetAttribLocation(programId, name)
    }

}
