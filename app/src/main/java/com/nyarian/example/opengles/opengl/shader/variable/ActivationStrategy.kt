package com.nyarian.example.opengles.opengl.shader.variable

import java.nio.Buffer

interface ActivationStrategy : LocatingStrategy {

    fun activate(location: Int, size: Int, type: Int, normalized: Boolean, stride: Int, ptr: Buffer)

}