package com.nyarian.example.opengles.opengl.attribute

fun coordinate(x: Float, y: Float) = TwoDCoordinate(x, y)
fun coordinate(x: Float, y: Float, z: Float) = ThreeDCoordinate(x, y, z)
fun coordinate(x: Float, y: Float, z: Float, w: Float) = FourDCoordinate(x, y, z, w)
