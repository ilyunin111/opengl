package com.nyarian.example.opengles.opengl.vertex

import com.nyarian.example.opengles.opengl.attribute.Attribute

class AttributesTypeCheck(private val attributes: Array<out Attribute>) {

    fun perform() {
        if (attributes.asSequence().map(Any::javaClass).distinct().count() != 1) {
            throw IllegalArgumentException(
                "Differently typed attributes detected: ${attributes.contentToString()}"
            )
        }
    }

}
