package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform1i

class Uniform1i(private val x: Int) : Uniform {

    override fun point(location: Int) {
        glUniform1i(location, x)
    }

}