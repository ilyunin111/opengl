package com.nyarian.example.opengles.opengl.shader.variable

import com.nyarian.example.opengles.opengl.shader.variable.attribute.AttributeLocatingStrategy
import com.nyarian.example.opengles.opengl.shader.variable.uniform.UniformLocatingStrategy

class LocatingStrategyFactory {

    fun create(variableType: VariableType): LocatingStrategy = when (variableType) {
        VariableType.UNIFORM -> UniformLocatingStrategy()
        VariableType.ATTRIBUTE, VariableType.VARYING -> AttributeLocatingStrategy()
    }

}
