package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glGetUniformLocation
import com.nyarian.example.opengles.opengl.shader.variable.LocatingStrategy

class UniformLocatingStrategy : LocatingStrategy {

    override fun getLocation(programId: Int, name: String): Int {
        return glGetUniformLocation(programId, name)
    }

}
