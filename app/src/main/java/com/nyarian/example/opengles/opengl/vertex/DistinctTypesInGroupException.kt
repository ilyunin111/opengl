package com.nyarian.example.opengles.opengl.vertex

class DistinctTypesInGroupException(message: String) : RuntimeException(message)
