package com.nyarian.example.opengles.opengl.shader

class IllegalShaderTypeException(message: String) : IllegalStateException(message)
