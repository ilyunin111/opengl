package com.nyarian.example.opengles

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.nyarian.example.opengles.feature.hockey.AirHockeyActivity
import com.nyarian.example.opengles.feature.part1.Part1Activity
import com.nyarian.example.opengles.feature.source_hockey.android.SourceAirHockeyActivity
import com.nyarian.example.opengles.feature.startandroid.intro.IntroActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        part1.setOnClickListener { startActivity(Part1Activity::class.java) }
        airHockey.setOnClickListener { startActivity(AirHockeyActivity::class.java) }
        startAndroidIntro.setOnClickListener { startActivity(IntroActivity::class.java) }
        sourceHockey.setOnClickListener { startActivity(SourceAirHockeyActivity::class.java) }
    }

    private fun <T : Activity> startActivity(clazz: Class<T>) {
        startActivity(Intent(this, clazz))
    }
}
