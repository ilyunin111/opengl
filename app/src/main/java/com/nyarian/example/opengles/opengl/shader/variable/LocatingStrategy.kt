package com.nyarian.example.opengles.opengl.shader.variable

interface LocatingStrategy {

    fun getLocation(programId: Int, name: String): Int

}