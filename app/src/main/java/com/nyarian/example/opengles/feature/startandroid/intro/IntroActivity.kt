package com.nyarian.example.opengles.feature.startandroid.intro

import android.app.ActivityManager
import android.content.Context
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.nyarian.example.opengles.R
import kotlinx.android.synthetic.main.activity_startandroid_intro.*

class IntroActivity : AppCompatActivity() {

    private var glSurfaceView: GLSurfaceView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!supportES2()) {
            Toast.makeText(this, "OpenGl ES 2.0 is not supported", Toast.LENGTH_LONG).show()
            finish()
        }
        setContentView(R.layout.activity_startandroid_intro)
        introScene.setOnClickListener { setGlView(IntroRenderer(resources)) }
        complexScene.setOnClickListener { setGlView(ComplexSceneRenderer(resources)) }
        oneSevenOne.setOnClickListener { setGlView(SceneRenderer171(resources)) }
        oneSevenTwo.setOnClickListener { setGlView(SceneRenderer172(resources)) }
        oneSevenThree.setOnClickListener { setGlView(SceneRenderer173(resources)) }
        oneSevenFour.setOnClickListener { setGlView(SceneRenderer174(resources)) }
    }

    private fun setGlView(introRenderer: GLSurfaceView.Renderer) {
        val localSurfaceView = GLSurfaceView(this)
        localSurfaceView.setEGLContextClientVersion(2)
        localSurfaceView.setRenderer(introRenderer)
        setContentView(localSurfaceView)
        glSurfaceView = localSurfaceView
    }

    override fun onPause() {
        super.onPause()
        glSurfaceView?.onPause()
    }

    override fun onResume() {
        super.onResume()
        glSurfaceView?.onResume()
    }

    private fun supportES2(): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val configurationInfo = activityManager.deviceConfigurationInfo
        return configurationInfo.reqGlEsVersion >= 0x20000
    }

}
