package com.nyarian.example.opengles.opengl.scene

import android.opengl.GLES20.glDrawArrays
import com.nyarian.example.opengles.opengl.shader.variable.uniform.Uniform4f
import com.nyarian.example.opengles.opengl.shader.variable.uniform.UniformPointer
import com.nyarian.example.opengles.opengl.vertex.DrawableFigure
import com.nyarian.example.opengles.opengl.vertex.Merge
import com.nyarian.example.opengles.opengl.vertex.Figure
import com.nyarian.example.opengles.opengl.vertex.strides

class UniformGroup<T : Figure>(
    private val uniformPointer: UniformPointer,
    private val colorUniform: Uniform4f,
    private vararg val givenVertices: T
) : DrawableFigure {

    override val attributes: FloatArray by lazy { Merge(givenVertices).merged }
    override val type: Int
    override val vertices: Int
    override val components: Int
    override val stride: Int

    init {
        type = GlesTypesCheck(givenVertices).perform()
        components = ComponentsCheck(givenVertices).perform()
        stride = StrideCheck(sequenceOf(*givenVertices).strides()).perform()
        vertices = givenVertices
            .asSequence()
            .map(Figure::vertices)
            .sum()
    }

    override fun draw(offset: Int): Int {
        colorUniform.point(uniformPointer.pointer)
        glDrawArrays(type, offset, vertices)
        return vertices + offset
    }

}
