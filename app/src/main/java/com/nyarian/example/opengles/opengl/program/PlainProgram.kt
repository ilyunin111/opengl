package com.nyarian.example.opengles.opengl.program

import android.opengl.GLES20.*
import com.nyarian.example.opengles.opengl.check.CheckException
import com.nyarian.example.opengles.opengl.check.ProgramCheck
import com.nyarian.example.opengles.opengl.shader.CompiledShader
import com.nyarian.example.opengles.opengl.shader.ProgramCreationException
import com.nyarian.example.opengles.opengl.shader.ProgramLinkingException
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable.ActivationParameters
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.opengl.vertex.VertexBuffer

class PlainProgram(vertexShader: CompiledShader, fragmentShader: CompiledShader) : Program {

    private val id: Int = createProgram(vertexShader, fragmentShader)
    private val pointableVariables: Array<ShaderPointableVariable> =
        vertexShader.pointableVariables + fragmentShader.pointableVariables
    private val variables: Array<ShaderVariable> = vertexShader.variables + fragmentShader.variables

    init {
        pointableVariables.asSequence()
            .map(ShaderPointableVariable::name)
            .plus(variables.asSequence().map(ShaderVariable::name))
            .distinct()
            .count()
            .takeIf { it != pointableVariables.size + variables.size }
            ?.let {
                throw IllegalArgumentException(
                    "Shaders variables can't share names " +
                            "(${pointableVariables.contentToString()}, ${variables.contentToString()})"
                )
            }
    }

    private fun createProgram(vertexShader: CompiledShader, fragmentShader: CompiledShader): Int {
        val programId: Int = initializeProgram()
        glAttachShader(programId, vertexShader.id)
        glAttachShader(programId, fragmentShader.id)
        linkProgram(programId, vertexShader, fragmentShader)
        return programId
    }

    private fun initializeProgram(): Int {
        val programId = glCreateProgram()
        if (programId == 0) {
            throw ProgramCreationException("Could not create program")
        }
        return programId
    }

    private fun linkProgram(programId: Int, vertexShader: CompiledShader, fragmentShader: CompiledShader) {
        glLinkProgram(programId)
        checkLinkingResult(programId, vertexShader, fragmentShader)
    }

    private fun checkLinkingResult(programId: Int, vertexShader: CompiledShader, fragmentShader: CompiledShader) {
        try {
            ProgramCheck(programId, GL_LINK_STATUS).perform()
        } catch (e: CheckException) {
            glDetachShader(programId, vertexShader.id)
            glDetachShader(programId, fragmentShader.id)
            glDeleteProgram(programId)
            throw ProgramLinkingException(
                "Could not link a program ($programId) with shaders $vertexShader and $fragmentShader. " +
                        "OpenGL log: ${glGetProgramInfoLog(programId)}"
            )
        }
    }

    override fun validate() {
        try {
            ProgramCheck(id, GL_VALIDATE_STATUS).perform()
        } catch (e: CheckException) {
            throw ProgramValidationException("Program is not valid. Log: ${glGetProgramInfoLog(id)}")
        }
    }

    override fun use() {
        glUseProgram(id)
    }

    override fun pointVariable(name: String, parameters: ActivationParameters, vertexBuffer: VertexBuffer) {
        (pointableVariables.firstOrNull { it.name == name }
            ?: throw IllegalStateException("Variable with name $name was not found"))
            .adjustPointer(id, parameters, vertexBuffer)
    }

    override fun getVariableLocation(name: String): Int {
        return variables.first { it.name == name }.getLocation(id)
    }

}
