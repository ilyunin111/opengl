package com.nyarian.example.opengles.opengl.vertex

import android.opengl.GLES20.GL_POINTS
import com.nyarian.example.opengles.opengl.attribute.Attribute
import com.nyarian.example.opengles.opengl.attribute.coordinate

class Point(attribute: Attribute) : Figure {

    override val attributes: FloatArray = attribute.asFloatArray

    override val components: Int = attribute.components

    override val type: Int get() = GL_POINTS

    override val vertices: Int
        get() = POINT_VERTICES

    override val stride: Int = attribute.stride

    constructor(x: Float, y: Float) : this(coordinate(x, y))

    companion object {
        private const val POINT_VERTICES = 1
    }

}
