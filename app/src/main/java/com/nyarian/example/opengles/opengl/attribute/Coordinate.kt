package com.nyarian.example.opengles.opengl.attribute

interface Coordinate {

    val asFloatArray: FloatArray

}