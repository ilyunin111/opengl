package com.nyarian.example.opengles.opengl.shader.variable.uniform

import android.opengl.GLES20.glUniform2i

class Uniform2i(private val x: Int, private val y: Int) : Uniform {

    override fun point(location: Int) {
        glUniform2i(location, x, y)
    }

}