package com.nyarian.example.opengles.opengl.shader

class ShaderCompilationException(message: String) : RuntimeException(message)
