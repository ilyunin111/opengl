package com.nyarian.example.opengles.opengl.vertex

class Merge(figures: Array<out Figure>) {

    val merged: FloatArray by lazy {
        figures.asSequence().map(Figure::attributes)
            .fold(FloatArray(0)) { accumulator, next -> accumulator + next }
    }

}
