package com.nyarian.example.opengles.opengl.shader

import android.opengl.GLES20.GL_FRAGMENT_SHADER
import android.opengl.GLES20.GL_VERTEX_SHADER
import com.nyarian.example.opengles.opengl.shader.variable.ShaderPointableVariable
import com.nyarian.example.opengles.opengl.shader.variable.ShaderVariable
import com.nyarian.example.opengles.resource.TextResource

class Shader(
    private val code: String,
    private val type: Int,
    private val pointableVariables: Array<ShaderPointableVariable>,
    private val variables: Array<ShaderVariable>
) {

    fun compile() = CompiledShader(pointableVariables, variables, type, code)

    companion object {
        fun vertex(
            code: String, pointableVariables: Array<ShaderPointableVariable> = emptyArray(),
            variables: Array<ShaderVariable> = emptyArray()
        ) =
            Shader(code, GL_VERTEX_SHADER, pointableVariables, variables)

        fun vertex(
            textResource: TextResource,
            pointableVariables: Array<ShaderPointableVariable> = emptyArray(),
            variables: Array<ShaderVariable> = emptyArray()
        ) =
            vertex(textResource.asString(), pointableVariables, variables)

        fun fragment(
            code: String, pointableVariables: Array<ShaderPointableVariable> = emptyArray(),
            variables: Array<ShaderVariable> = emptyArray()
        ) =
            Shader(code, GL_FRAGMENT_SHADER, pointableVariables, variables)

        fun fragment(
            textResource: TextResource, pointableVariables: Array<ShaderPointableVariable> = emptyArray(),
            variables: Array<ShaderVariable> = emptyArray()
        ) =
            fragment(textResource.asString(), pointableVariables, variables)
    }

}