package com.nyarian.example.opengles.opengl.render

data class Coordinate(val x: Int, val y: Int) {

    companion object {
        val CENTER = Coordinate(0, 0)
    }

}