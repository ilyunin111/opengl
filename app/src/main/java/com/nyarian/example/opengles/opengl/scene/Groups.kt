package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.program.Program
import com.nyarian.example.opengles.opengl.shader.variable.uniform.Uniform4f
import com.nyarian.example.opengles.opengl.shader.variable.uniform.UniformPointer
import com.nyarian.example.opengles.opengl.vertex.DrawableFigure
import com.nyarian.example.opengles.opengl.vertex.Figure

fun Figure.group() = Group(this)

fun Figure.colorUniformGroup(pointer: UniformPointer, colorUniform: Uniform4f) =
    UniformGroup(pointer, colorUniform, this)

fun Figure.onDraw(onDraw: () -> Unit) = CallbackGroup(Group(this), onDraw)

fun DrawableFigure.onDraw(onDraw: () -> Unit) = CallbackGroup(this, onDraw)

fun DrawableFigure.withProgram(program: Program) = ProgramBoundGroup(program, this)

fun DrawableFigure.intrinsicOffset() = IntrinsicOffsetGroup(this)

fun DrawableFigure.resetOffset() = ResetOffsetGroup(this)
