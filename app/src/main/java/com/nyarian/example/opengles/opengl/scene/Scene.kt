package com.nyarian.example.opengles.opengl.scene

import com.nyarian.example.opengles.opengl.vertex.DrawableFigure
import com.nyarian.example.opengles.opengl.vertex.Merge
import com.nyarian.example.opengles.opengl.vertex.Figure

class Scene(private vararg val vertices: DrawableFigure) {

    val floatArray: FloatArray by lazy { Merge(vertices).merged }

    val components: Int by lazy { ComponentsCheck(vertices).perform() }

    val stride: Int by lazy { StrideCheck(vertices.asSequence().map(Figure::stride).toList().toIntArray()).perform() }

    fun draw() {
        vertices.fold(0) { totalOffset, vertex -> vertex.draw(totalOffset) }
    }

}
