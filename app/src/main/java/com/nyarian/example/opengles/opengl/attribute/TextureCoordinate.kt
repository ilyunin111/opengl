package com.nyarian.example.opengles.opengl.attribute

import com.nyarian.example.opengles.opengl.backbone.BYTES_PER_FLOAT

class TextureCoordinate(private val s: Float, private val t: Float) : Attribute, Coordinate {

    override val asFloatArray: FloatArray by lazy { floatArrayOf(s, t) }
    override val components: Int get() = COMPONENTS
    override val stride: Int get() = STRIDE

    companion object {
        const val COMPONENTS = 2
        const val STRIDE = COMPONENTS * BYTES_PER_FLOAT
    }

}