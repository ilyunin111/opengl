package com.nyarian.example.opengles.feature.part1

import android.opengl.GLES20.*
import android.opengl.GLSurfaceView
import java.util.*
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class FirstOpenGlProjectRenderer : GLSurfaceView.Renderer {

    val random = Random()

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        glClearColor(oneOrZero(), oneOrZero(), oneOrZero(), oneOrZero())
    }

    private fun oneOrZero() = if (random.nextBoolean()) 1.0f else 0.0f

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        glViewport(0, 0, width, height)
    }

    override fun onDrawFrame(gl: GL10?) {
        glClear(GL_COLOR_BUFFER_BIT)
    }

}
